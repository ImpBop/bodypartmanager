﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace BodyPartManager
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ManagerChunksBPM))]
    public class ManagerChunksEditor : Editor
    {
        private ManagerChunksBPM _chunkM;
        private ManagerChunksBPM chunkM
        {
            get
            {
                if (_chunkM == null)
                    _chunkM = target as ManagerChunksBPM;
                return _chunkM;
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.Space(20);

            if (GUILayout.Button((chunkM.chunksTabShown ? "Hide" : "Show") + " Chunk Settings"))
                chunkM.ToggleChunkSettingsTab();

            if (chunkM.chunksTabShown)
            {
                ShowBodyChunksList();
                GUILayout.Space(20);
                SetupBodyChunks();
            }
        }

        private void SetupBodyChunks()
        {
            GUI.color = Color.green;
            if (GUILayout.Button("Update Body Chunks"))
            {
                chunkM.MakeChunksMesh();
            }
            GUI.color = Color.white;
            EditorUtility.SetDirty(target);
        }


        private void ShowBodyChunksList()
        {
            EditorGUI.BeginChangeCheck();
            for (int i = 0; i < chunkM.bodyChunksObjects.Count; i++)
            {
                ManagerChunksBPM.BodyChunkSettings bcs = chunkM.bodyChunksObjects[i];

                GUI.color = chunkM.ChunkStatusIsInComplete(bcs) ? Color.red : Color.green;
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                GUI.color = Color.white;
                GUILayout.BeginHorizontal();
                GUILayout.Label("Body Chunk");
                bcs.bodyChunk = (GameObject)EditorGUILayout.ObjectField(bcs.bodyChunk, typeof(GameObject), true);
                GUILayout.Label("Skinned Mesh");
                bcs.skinnedMeshParent = (SkinnedMeshRenderer)EditorGUILayout.ObjectField(bcs.skinnedMeshParent, typeof(SkinnedMeshRenderer), true);
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                int bpCount = (bcs.connectedBodyParts != null ? bcs.connectedBodyParts.Count : 0);
                if (bpCount > 0)
                {
                    GUILayout.Label("Can be Shot");
                    bcs.canBeShot = EditorGUILayout.Toggle(bcs.canBeShot);
                    GUILayout.Label("Keep BodyPart" + (bpCount > 1 ? "s" : ""));
                    bcs.keepBodyPart = EditorGUILayout.Toggle(bcs.keepBodyPart);
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginVertical();
                for (int b = 0; b < bcs.connectedBodyParts.Count; b++)
                {
                    GUILayout.BeginHorizontal();
                    bcs.connectedBodyParts[b] = (BodyPartBPM)EditorGUILayout.ObjectField(bcs.connectedBodyParts[b], typeof(BodyPartBPM), true);
                    if (GUILayout.Button("Remove BodyPart"))
                    {
                        bcs.connectedBodyParts.RemoveAt(b);
                        return;
                    }
                    GUILayout.EndHorizontal();
                }

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Add Connected BodyPart"))
                {
                    bcs.connectedBodyParts.Add(null);
                    return;
                }

                GUI.color = Color.red;
                if (GUILayout.Button("Remove/Unlink Chunk"))
                {
                    chunkM.bodyChunksObjects.RemoveAt(i);
                    return;
                }
                GUI.color = Color.white;
                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
                EditorGUILayout.EndVertical();
                GUILayout.Space(10);
            }

            if (GUILayout.Button("Add New Chunk"))
            {
                chunkM.bodyChunksObjects.Add(new ManagerChunksBPM.BodyChunkSettings());
                return;
            }

            EditorUtility.SetDirty(target);
        }
    }
}
#endif
