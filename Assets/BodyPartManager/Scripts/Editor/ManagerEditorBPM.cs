﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace BodyPartManager
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ManagerBPM))]
    public class BodyPartManagerEditor : Editor
    {

        ReorderableList m_bodyList = null;
        private ManagerBPM _manager;
        private ManagerBPM manager
        {
            get
            {
                if (_manager == null)
                    _manager = target as ManagerBPM;
                return _manager;
            }
        }

        private void OnEnable()
        {
            m_bodyList = new ReorderableList(manager.bodySettingsList, typeof(ManagerBPM), true, true, true, true);

            m_bodyList.drawHeaderCallback += DrawHeader;
            m_bodyList.drawElementCallback += DrawElement;

            m_bodyList.onAddCallback += AddItem;
            m_bodyList.onRemoveCallback += RemoveItem;
        }

        #region BodyPartsDisplay
        private void DrawHeader(Rect rect)
        {
            maxDepth = manager.GetMaxDepth();

            //GLOBAL 
            widthProcentage = rect.width / 100f;
            depthWidth = 5f * widthProcentage;    //10% width
            typeWidth = 15f * widthProcentage;    //10% width

            hierarchyWidth = 80 * widthProcentage;     //80% width 
            minhierarchyWidth = 0.5f * hierarchyWidth; // 50% width of hierarchyWidth
            hierarchyChunck = (hierarchyWidth - minhierarchyWidth) / (maxDepth + 1);

            GUI.Label(rect, "BodyPart List");
        }

        private float widthProcentage, depthWidth, typeWidth;
        private float hierarchyWidth, minhierarchyWidth, hierarchyChunck;
        private int maxDepth = 1;
        private void DrawElement(Rect rect, int index, bool active, bool focused)
        {
            ManagerBPM.BodySettings item = manager.bodySettingsList[index];

            EditorGUI.BeginChangeCheck();
            item.depth = EditorGUI.IntField(new Rect(rect.x, rect.y, depthWidth, rect.height), item.depth);
            item.type = (Constants.BODYPART_TYPE)EditorGUI.EnumPopup(new Rect(rect.x + depthWidth, rect.y, typeWidth, rect.height), item.type);

            //SPECIFIC    
            float myWidth = (((maxDepth + 1) - (item.depth)) * hierarchyChunck) + minhierarchyWidth;
            float myIndent = hierarchyWidth - (myWidth);
            item.bone = (Transform)EditorGUI.ObjectField(new Rect(rect.x + depthWidth + typeWidth + myIndent, rect.y, myWidth, rect.height), item.bone, typeof(Transform), true);

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(target);
            }
        }

        private void AddItem(ReorderableList list)
        {
            m_bodyList.list.Add(new ManagerBPM.BodySettings());

            EditorUtility.SetDirty(target);
        }

        private void RemoveItem(ReorderableList list)
        {
            m_bodyList.list.RemoveAt(list.index);
            EditorUtility.SetDirty(target);
        }

        private void ControlButtons()
        {
            GUILayout.Space(5);

            GUI.color = Color.red;
            if (GUILayout.Button("Clear All Components and Connections"))
            {
                manager.ClearAll();
                return;
            }

            GUI.color = Color.green;
            if (manager.bodySettingsList.Count > 0)
            {
                if (GUILayout.Button("Generate / Update"))
                {
                    manager.CreateFromList();
                    return;
                }
            }

            GUI.color = Color.white;
        }

        private void Connectors()
        {
            //EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Box("Connectors List: RigidBody > Bone", EditorStyles.largeLabel);
            EditorGUILayout.HelpBox("Assign the bones to connect it to the RigidBody, Update when you update a new connection or when you move or rotate (a) connected RigidBody(s)", MessageType.Info);

            EditorGUI.BeginChangeCheck();
            bool canClear = false;

            for (int i = 0; i < manager.bodySettingsList.Count; i++)
            {
                if (manager.bodySettingsList[i].connectors == null)
                {
                    Color yellowish = (Color.yellow * .5f) + (Color.white * .5f);
                    GUI.color = yellowish;
                    break;
                }
            }

            for (int i = 0; i < manager.bodySettingsList.Count; i++)
            {
                GUILayout.BeginHorizontal();
                if (manager.bodySettingsList[i] != null)
                {
                    if (manager.bodySettingsList[i].bone != null)
                        EditorGUILayout.TextField(manager.bodySettingsList[i].bone.transform.name, GUILayout.MaxWidth(128));
                    manager.bodySettingsList[i].connectors = (Transform)EditorGUILayout.ObjectField(manager.bodySettingsList[i].connectors, typeof(Transform), true);
                    if (manager.bodySettingsList[i].connectors != null) canClear = true;
                }
                GUILayout.EndVertical();
            }

            if (manager.connectors.Count > 0 || canClear)
            {
                GUI.color = Color.red;
                if (GUILayout.Button("Clear Connectors"))
                {
                    manager.ConnectorsList(true);
                    return;
                }
            }

            if (manager.bodySettingsList.Count > 0)
            {
                bool needsUpdate = manager.connectors.Count == 0;

                GUI.color = needsUpdate ? Color.yellow : Color.green;
                if (GUILayout.Button((manager.connectors.Count > 0) ? "Update Connectors" : "Link Connectors"))
                {
                    manager.ConnectorsList();
                    return;
                }
                GUI.color = Color.white;
            }
            EditorUtility.SetDirty(target);
        }
        #endregion

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.Space(20);
            if (GUILayout.Button((manager.connectionTabShown ? "Hide" : "Show") + " Connection Settings"))
                manager.ToggleConnectionSettingsTab();
            if (manager.connectionTabShown)
            {
                GUILayout.Box("RigidBody Hierarchy", EditorStyles.largeLabel);
                EditorGUILayout.HelpBox("Setup for the RigidBody Hierarchy assign the RigidBodies to this list, Note: keep in mind these will be connected to the bones at the Connectors List below.", MessageType.Info);

                for (int i = 0; i < manager.bodySettingsList.Count; i++)
                {
                    if (manager.bodySettingsList[i].bone == null)
                    {
                        Color yellowish = (Color.yellow * .5f) + (Color.white * .5f);
                        GUI.color = yellowish;
                        break;
                    }
                }
                m_bodyList.DoLayoutList();
                ControlButtons();

                GUILayout.Space(20);
                Connectors();
            }
        }
    }
}
#endif
