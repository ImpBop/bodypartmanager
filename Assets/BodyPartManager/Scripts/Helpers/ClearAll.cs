﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ClearAll : MonoBehaviour
{
    #if UNITY_EDITOR
    public void ClearAllComponents ()
    {
        //fist clear all the joints, these are depended on the rigid-bodies.
        Joint[] joints = transform.GetComponentsInChildren<Joint>();
        foreach (Joint joint in joints)
            DestroyImmediate(joint);

        Transform[] childeren = transform.GetComponentsInChildren<Transform>();
        List<GameObject> selection = new List<GameObject>();
        foreach (Transform child in childeren)
        {
            //don't remove components from self
            if (child != transform)
            {
                Component[] components = child.gameObject.GetComponents<Component>();
                foreach (Component comp in components)
                {
                    if (comp == null)
                    {
                        Debug.Log("Found missing script. adding to selection");
                        selection.Add(child.gameObject);
                    }
                    else if (!(comp is Transform))
                    {
                        DestroyImmediate(comp);
                    }
                }
            }
        }
        Selection.objects = selection.ToArray();
    }
    #endif
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(ClearAll))]
public class ClearAllEditor : Editor
{
    private ClearAll _clear;
    private ClearAll clear
    {
        get
        {
            if (_clear == null)
                _clear = target as ClearAll;
            return _clear;
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Space(20);

        if (GUILayout.Button("Clear All Components"))
            clear.ClearAllComponents();
    }
}
#endif