﻿using UnityEngine;
using BodyPartManager;

public class GUITesterBPM : MonoBehaviour
{
    private ManagerBPM[] _managerBPMs;
    private Camera _camera;

    private const int ruleHeight = 40;
    private const int guiWidth = 300;
    private readonly Vector2 cursorSize = new Vector2(150,150);

    [SerializeField] private Texture2D _cursor; 

    [Header("Impact Setting")]
    [SerializeField] private float _shotDamage;

    protected void Start()
    {
        _managerBPMs = FindObjectsOfType<ManagerBPM>() as ManagerBPM[];
        _camera = Camera.main;
    }

    private void OnGUI()
    {
        if (_managerBPMs == null || _managerBPMs.Length == 0)
            return;

        for (int i =0; i < _managerBPMs.Length; i++)
        {
            if (_managerBPMs[i].currentState == Constants.BODYSTATE.ANIMATION)
            {
                if (GUI.Button(new Rect(new Vector2(0, ruleHeight * i), new Vector2(guiWidth, ruleHeight)), "Trigger Ragdoll"))
                {
                _managerBPMs[i].currentState = Constants.BODYSTATE.RAGDOLL;
                }
            }
            else if (_managerBPMs[i].currentState == Constants.BODYSTATE.RAGDOLL)
            {    
                if (GUI.Button(new Rect(new Vector2(0, ruleHeight * i), new Vector2(guiWidth, ruleHeight)), "Trigger Animation"))
                {
                    _managerBPMs[i].currentState = Constants.BODYSTATE.ANIMATION;
                }
            }
        }

        Vector2 mousePos = Input.mousePosition;
        mousePos.y = Screen.height - mousePos.y;
        GUI.Label(new Rect(mousePos - cursorSize * .5f, cursorSize), _cursor);
        if (Input.GetMouseButtonDown(0))
            Shoot();
    }

    private void Shoot()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 500))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.green, .5f);
            BodyPartBPM bodyPart = hit.collider.transform.GetComponent<BodyPartBPM>();
            //Check if we have hit an BodyPart
            if (bodyPart != null)
            {
                Constants.ImpactInfo normalShot = new Constants.ImpactInfo(_camera.transform, _shotDamage);
                    
                //instantly define the impact, normally we would wait a few frames
                normalShot.OnImpact(hit.point);
                bodyPart.isShot(normalShot, true);
            }
        }
        else
            Debug.DrawRay(ray.origin, ray.direction * 900, Color.red, 0.5f);
    }

}
