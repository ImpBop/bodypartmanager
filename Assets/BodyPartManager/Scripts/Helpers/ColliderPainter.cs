﻿using UnityEngine;

public class ColliderPainter : MonoBehaviour
{
    public bool DrawOnlyWhenSelected;


    public Color GizmoColorDefault = Color.yellow;
    public Color GizmoColorSelected = Color.green;
    [Range(0, 1)]
    public float GizmosAlpha = .5f;


    //check update state of collider
    private bool _hasCollider;
    private Collider _collider;

    private BoxCollider _boxCollider;
    private SphereCollider _sphereCollider;
    private CapsuleCollider _capsuleCollider;


    private void OnDrawGizmos()
    {
        if (!DrawOnlyWhenSelected)
            DrawCollider();
    }
    private void OnDrawGizmosSelected()
    {
        if (DrawOnlyWhenSelected)
            DrawCollider();
    }

    private void DrawBox(Color selectionColor)
    {
        Gizmos.color = selectionColor;
        Gizmos.DrawWireCube(Vector3.zero + _boxCollider.center, _boxCollider.size);

        selectionColor.a = GizmosAlpha;
        Gizmos.color = selectionColor;
        Gizmos.DrawCube(Vector3.zero + _boxCollider.center, _boxCollider.size);
    }
    private void DrawSphere(Color selectionColor)
    {
        Gizmos.color = selectionColor;
        Gizmos.DrawWireSphere(Vector3.zero + _sphereCollider.center, _sphereCollider.radius);

        selectionColor.a = GizmosAlpha;
        Gizmos.color = selectionColor;
        Gizmos.DrawSphere(Vector3.zero + _sphereCollider.center, _sphereCollider.radius);
    }

    private readonly Vector3[] directions = new Vector3[3] { Vector3.right, Vector3.up, Vector3.forward };
    private void DrawCapsule(Color selectionColor)
    {
        Gizmos.color = selectionColor;
        float heightOffset = (_capsuleCollider.height * .5f) - _capsuleCollider.radius;
        if (_capsuleCollider.height <= _capsuleCollider.radius * 2)
            heightOffset = 0;

        Vector3 realtiveOffset = directions[_capsuleCollider.direction] * heightOffset;
        Vector3 center = _capsuleCollider.center;

        Gizmos.DrawWireSphere(center + realtiveOffset, _capsuleCollider.radius);
        Gizmos.DrawWireSphere(center + -realtiveOffset, _capsuleCollider.radius);

        if (_capsuleCollider.direction != 0)
        {
            Gizmos.DrawLine(center + realtiveOffset + (Vector3.left * _capsuleCollider.radius),
                            center + -realtiveOffset + (Vector3.left * _capsuleCollider.radius));
            Gizmos.DrawLine(center + realtiveOffset + (Vector3.right * _capsuleCollider.radius),
                            center + -realtiveOffset + (Vector3.right * _capsuleCollider.radius));
        }
        if (_capsuleCollider.direction != 1)
        {
            Gizmos.DrawLine(center + realtiveOffset + (Vector3.up * _capsuleCollider.radius),
                            center + -realtiveOffset + (Vector3.up * _capsuleCollider.radius));
            Gizmos.DrawLine(center + realtiveOffset + (Vector3.down * _capsuleCollider.radius),
                            center + -realtiveOffset + (Vector3.down * _capsuleCollider.radius));
        }
        if (_capsuleCollider.direction != 2)
        {
            Gizmos.DrawLine(center + realtiveOffset + (Vector3.forward * _capsuleCollider.radius),
                            center + -realtiveOffset + (Vector3.forward * _capsuleCollider.radius));
            Gizmos.DrawLine(center + realtiveOffset + (Vector3.back * _capsuleCollider.radius),
                            center + -realtiveOffset + (Vector3.back * _capsuleCollider.radius));
        }

        selectionColor.a = GizmosAlpha;
        Gizmos.color = selectionColor;
        Gizmos.DrawSphere(center + realtiveOffset, _capsuleCollider.radius);
        Gizmos.DrawSphere(center + -realtiveOffset, _capsuleCollider.radius);
    }

    private void DrawCollider()
    {
        //Check if the collider has updated
        if (_hasCollider && _collider == null)
        {
            _hasCollider = false;
            return;
        }
        else if (!_hasCollider)
        {
            _collider = GetComponent<Collider>();
            if (_collider != null)
            {
                _hasCollider = true;
                _boxCollider = GetComponent<BoxCollider>();
                _sphereCollider = GetComponent<SphereCollider>();
                _capsuleCollider = GetComponent<CapsuleCollider>();
            }
            else
            {
                return;
            }
        }

        Gizmos.matrix = transform.localToWorldMatrix;
        bool selected = UnityEditor.Selection.Contains(gameObject);
        Color selectionColor = selected ? GizmoColorSelected : GizmoColorDefault;

        if (_boxCollider != null)
            DrawBox(selectionColor);
        else if (_sphereCollider != null)
            DrawSphere(selectionColor);
        else if (_capsuleCollider != null)
            DrawCapsule(selectionColor);
    }
}
