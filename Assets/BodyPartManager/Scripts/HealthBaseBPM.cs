﻿using UnityEngine;

namespace BodyPartManager
{ 
    [RequireComponent(typeof(ManagerBPM))]
    public class HealthBaseBPM : MonoBehaviour
    {
        public ManagerBPM Manager { private set; get; }
        public ManagerChunksBPM ChunkManager { private set; get; }

        public bool IsDead { get { return Health <= 0; } }
        protected float _health;
        public float Health { get { return _health; } }

        public float startHealth = 100;
        public int maxLimbsDetached = 2;
            private int currentDetachedLimbs = 0;

        private Transform _rootBone;

        private void Awake()
        {
            ResetHealthBase();
        }

        private void OnDeath()
        {
            //Could add the event for the AI here to handle the Dead,
        }

        private void RevievedShotImpact()
        {
            //Could add the event for the AI here to handle the shot,
            //Also triggering of flinch animations would be logic to call from here.
        }

        private void HandleBodyPartImpact(Constants.ImpactInfo impactInfo, BodyPartBPM bodypart)
        {
            if (bodypart == null)
                return;

            if (ChunkManager != null)
            {
                //check if we can detach this body-part, if not give the ragdoll force
                //if so check maxLimbsDetached
                if (!ChunkManager.CheckDetachBodyPart(bodypart, impactInfo.GetImpactForce()))
                    bodypart.myRigidBody.AddForce(impactInfo.GetImpactForce());
                else
                {
                    currentDetachedLimbs++;
                    if (currentDetachedLimbs >= maxLimbsDetached)
                        GlobalImpact(impactInfo, true);
                }
            }
            else
            {
              bodypart.myRigidBody.AddForce(impactInfo.GetImpactForce());
            }
        }

        private void HandleDeath(Constants.ImpactInfo impactInfo)
        {
            _health = 0;
            OnDeath();

            if (impactInfo.IsExplosionForce)
            {
                TryExplosion(impactInfo);
            }
            else
            {
                Manager.ActivateRagdoll();
                Rigidbody rb = Manager.bodyParts[0].myRigidBody;
                rb.AddForceAtPosition(impactInfo.GetImpactForce(), impactInfo.ImpactPoint);
            }
        }

        private void TryExplosion(Constants.ImpactInfo impactInfo)
        {
            if (ChunkManager != null)
            {
                if (!ChunkManager.isExploded)
                {
                    Manager.ActivateRagdoll();
                    ChunkManager.Explode(impactInfo.GetRadiusForce(_rootBone.position), impactInfo.ImpactPoint, impactInfo.ExplosionRadius);
                }
            }
            else
            {
                Manager.ActivateRagdoll();
                // no explosion for chunks so add force to the main body
                Rigidbody rb = Manager.bodyParts[0].myRigidBody;
                rb.AddForceAtPosition(impactInfo.GetImpactForce(), impactInfo.ImpactPoint);
            }
        }

        public virtual void ResetHealthBase()
        {
            _health = startHealth;
            currentDetachedLimbs = 0;

            Manager = GetComponent<ManagerBPM>();
            if (Manager != null)
                Manager.ResetBodyPartManager();

            //check if there is a Chunk Manager attached, if so reset it!
            ChunkManager = GetComponent<ManagerChunksBPM>();
            if (ChunkManager != null)
                ChunkManager.ResetChunks();

            _rootBone = Manager.bodyParts[0].transform;

            //DeadTimePassed, GetUp invoke
            CancelInvoke(); 
        }

        public void GlobalImpact(Constants.ImpactInfo impactInfo, bool instantDeath)
        {
            BodyPartHit(impactInfo, null, instantDeath);
        }

        public virtual void BodyPartHit(Constants.ImpactInfo impactInfo, BodyPartBPM bodypart, bool instantDeath = false)
        {
            //impact handling of the rigid-bodies and checking if it hit a linked chunk
            if (bodypart != null)
                HandleBodyPartImpact(impactInfo, bodypart);

            RevievedShotImpact();

            if (impactInfo.IsExplosionForce && !instantDeath)
            {
                _health -= impactInfo.GetRadiusForce(_rootBone.position);
            }
            else
            {
                _health -= instantDeath ? _health : impactInfo.DamageForce;
            }

            if (_health <= 0)
                HandleDeath(impactInfo);
        }
    }
}