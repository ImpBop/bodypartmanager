﻿using UnityEngine;
using System.Collections.Generic;


namespace BodyPartManager
{
    [RequireComponent(typeof(ManagerBPM))]
    public class ManagerChunksBPM : MonoBehaviour
    {
        public bool isEnabled = true;

        public float defaultMass = 2f;
        public float defaultDrag = .01f;
        public float defaultAngularDrag = .05f;

        public Transform rootBone;
        private Transform chunksContainer;
        public Transform initialContainer;


        [System.Serializable]
        public class BodyChunkSettings
        {
            public bool canBeShot = false;
            public bool keepBodyPart = false;
            public GameObject bodyChunk;
            public SkinnedMeshRenderer skinnedMeshParent;
            public List<BodyPartBPM> connectedBodyParts = new List<BodyPartBPM>();

            //Not shown
            public Rigidbody rigidBody;
            public bool isActivated = false;
        }
        [HideInInspector]
        public List<BodyChunkSettings> bodyChunksObjects = new List<BodyChunkSettings>();

        public bool isExploded { get; private set; }

        private void Awake()
        {

            GameObject container = GameObject.Find("ChunksContainer");
            if (container == null)
                container = new GameObject("ChunksContainer");
            chunksContainer = container.transform;

            ResetChunks();
        }

        private BodyChunkSettings CheckBodyPartsInChunks(BodyPartBPM part, bool checkCanBeShot)
        {
            for (int i = 0; i < bodyChunksObjects.Count; i++)
            {
                if (!bodyChunksObjects[i].isActivated) //skip if its already activated
                {
                    if (!checkCanBeShot || bodyChunksObjects[i].canBeShot)
                    {
                        for (int j = 0; j < bodyChunksObjects[i].connectedBodyParts.Count; j++)
                        {
                            if (bodyChunksObjects[i].connectedBodyParts[j] == part)
                                return bodyChunksObjects[i];
                        }
                    }
                }
            }
            return null;
        }

        private void ActivateChunk(BodyChunkSettings chunk)
        {
            if (chunk.isActivated)
                return;

            chunk.bodyChunk.transform.rotation = rootBone.rotation * (chunk.skinnedMeshParent.rootBone.rotation);
            chunk.bodyChunk.transform.position = rootBone.position;

            for (int i = 0; i < chunk.connectedBodyParts.Count; i++)
            {
                //chunk.connectedBodyParts[i].ActivatePhysics(false);
                if (!chunk.keepBodyPart)
                    chunk.connectedBodyParts[i].ActivateBodyPart(false);
                //chunk.connectedBodyParts[i].gameObject.SetActive(false);
            }

            chunk.bodyChunk.transform.SetParent(chunksContainer);
            chunk.bodyChunk.SetActive(true);
            chunk.skinnedMeshParent.gameObject.SetActive(false);

            chunk.isActivated = true;
        }

        public void ResetChunks()
        {
            for (int i = 0; i < bodyChunksObjects.Count; i++)
            {
                BodyChunkSettings bcs = bodyChunksObjects[i];
                bcs.bodyChunk.transform.position = bcs.skinnedMeshParent.transform.position;
                bcs.bodyChunk.transform.rotation = bcs.skinnedMeshParent.transform.rotation;

                for (int c = 0; c < bcs.connectedBodyParts.Count; c++)
                {

                    bcs.connectedBodyParts[c].ActivatePhysics(false);
                    bcs.connectedBodyParts[c].ActivateBodyPart(true);
                    //bcs.connectedBodyParts[c].gameObject.SetActive(true);
                }

                bcs.skinnedMeshParent.gameObject.SetActive(true);
                bcs.bodyChunk.SetActive(false);
                bcs.bodyChunk.transform.SetParent(initialContainer);

                bcs.isActivated = false;
            }
            isExploded = false;
        }

        public void Explode(float force, Vector3 position, float radius)
        {
            if (isExploded || !isEnabled)
                return;

            for (int i = 0; i < bodyChunksObjects.Count; i++)
            {
                BodyChunkSettings bcs = bodyChunksObjects[i];
                if (!bcs.isActivated)
                {
                    ActivateChunk(bcs);
                    bcs.rigidBody.AddExplosionForce(force, position, radius);
                }
            }
            isExploded = true;
        }

        /// <summary>
        /// Check if the body part is inside the Chunks Manager, if so return true and detach the body part
        /// if not, then do nothing and return false.
        /// </summary>
        public bool CheckDetachBodyPart(BodyPartBPM bodyPart, Vector3 impactForce)
        {
            if (!isEnabled)
                return false;

            BodyChunkSettings bcs = CheckBodyPartsInChunks(bodyPart, true);
            if (bcs != null)
            {
                ActivateChunk(bcs);
                bcs.rigidBody.AddForce(impactForce);
                return true;
            }
            else
                return false;
        }

#if UNITY_EDITOR
        public bool chunksTabShown { get; private set; }
        public void ToggleChunkSettingsTab()
        {
            chunksTabShown = !chunksTabShown;
        }

        public void MakeChunksMesh()
        {
            for (int i = 0; i < bodyChunksObjects.Count; i++)
            {
                BodyChunkSettings bcs = bodyChunksObjects[i];

                MeshFilter meshF = bcs.bodyChunk.GetComponent<MeshFilter>();
                if (meshF == null)
                    meshF = bcs.bodyChunk.AddComponent<MeshFilter>();


                MeshRenderer meshR = bcs.bodyChunk.GetComponent<MeshRenderer>();
                if (meshR == null)
                    meshR = bcs.bodyChunk.AddComponent<MeshRenderer>();

                meshF.mesh = bcs.skinnedMeshParent.sharedMesh;
                meshR.material = bcs.skinnedMeshParent.sharedMaterial;


                SkinnedMeshRenderer skinned = bcs.bodyChunk.GetComponent<SkinnedMeshRenderer>();
                if (skinned != null)
                    DestroyImmediate(skinned);


                Rigidbody rigid = bcs.bodyChunk.GetComponent<Rigidbody>();
                if (rigid == null)
                    rigid = bcs.bodyChunk.AddComponent<Rigidbody>();

                bcs.rigidBody = rigid; //assign the rigidbody for later calculations;
                rigid.mass = defaultMass;
                rigid.drag = defaultDrag;
                rigid.angularDrag = defaultAngularDrag;

                /*
                //remove all PhysicsProp before adding new
                PhysicsProp[] physProps = bcs.bodyChunk.GetComponentsInChildren<PhysicsProp>();
                foreach (PhysicsProp physProp in physProps)
                    DestroyImmediate(physProp);

                Collider[] colliders = bcs.bodyChunk.GetComponentsInChildren<Collider>();
                foreach (Collider collider in colliders)
                {
                    PhysicsProp phyProp = collider.gameObject.GetComponent<PhysicsProp>();
                    if (phyProp == null)
                    {
                        phyProp = collider.gameObject.AddComponent<PhysicsProp>();
                        phyProp.SetRigidBody(rigid);
                    }
                }

                if (bcs.bodyChunk.GetComponent<Collider>() != null)
                {
                    PhysicsProp phyProp = bcs.bodyChunk.GetComponent<PhysicsProp>();
                    if (phyProp == null)
                        phyProp = bcs.bodyChunk.AddComponent<PhysicsProp>();
                }
                else if (bcs.bodyChunk.GetComponent<PhysicsProp>() != null)
                    DestroyImmediate(bcs.bodyChunk.GetComponent<PhysicsProp>());
                */
            }
        }

        public bool ChunkStatusIsInComplete(ManagerChunksBPM.BodyChunkSettings bcs)
        {
            if (bcs == null || bcs.bodyChunk == null || bcs.skinnedMeshParent == null)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < bcs.connectedBodyParts.Count; i++)
                {
                    if (bcs.connectedBodyParts[i] == null)
                        return true;
                }
            }
            return false;
        }
    #endif
    }
}

