﻿using UnityEngine;
using System.Collections.Generic;

namespace BodyPartManager
{
    public class ManagerBPM : MonoBehaviour
    {
        //disable all movement and calculations or resume when un-paused.
        public bool IsPaused { get; private set; }
        public bool IsRagdoll { private set; get; }

        public Constants.BODYSTATE currentState = Constants.BODYSTATE.NONE;
        public bool RigidSnapWhileInAnimation = true;

        public Transform rootTransform;
        public Transform RigidHipTransform;
        public Transform RigidHeadTransform;

        [Header("Animations")]
        public Animator animator;
        public float ragdollToAnimBlendTime = 1f;

        [Header("Debug")]
        public bool showWireAmature;
        public bool showWireConnectors;

        //Lists to make and connect the body parts and their bones
        [HideInInspector]
        public List<BodySettings> bodySettingsList = new List<BodySettings>();
        [HideInInspector]
        public List<BodyPartBPM> bodyParts = new List<BodyPartBPM>();
        [HideInInspector]
        public List<Connection> connectors = new List<Connection>();

        //global position of the rag-doll and the animation
        //private int rootRigidBone = 0;
        private float intRagdollBlendTime = 0;
        private bool snapShotTaken = false;
        private Transform _transform;
        private Vector3 glPOSITION;
        private Quaternion glROTATION;

        public delegate void GlobalHit(Constants.ImpactInfo impactInfo, bool instantDeath = false);

        [System.Serializable]
        public class Connection
        {
            public Transform bone;
            public Vector3 offsetPosition;
            public Quaternion offsetRotation;
            public Connection(Transform b, Vector3 oP, Quaternion oR)
            {
                bone = b;
                offsetPosition = oP;
                offsetRotation = oR;
            }
        }
        [System.Serializable]
        public class BodySettings
        {
            public Transform bone;
            public Transform connectors;
            public Constants.BODYPART_TYPE type;
            public int depth;
        }

        private void Awake()
        {
            ResetBodyPartManager();
        }

        private void Update()
        {
            if (IsPaused)
                return;

            //save the position and rotation of the parent
            glPOSITION = rootTransform.position;
            glROTATION = rootTransform.rotation;

            if (currentState == Constants.BODYSTATE.RAGDOLL && IsRagdoll)
            {
                SnapArmatureToRigidBones();
            }
        }

        private void LateUpdate()
        {
            if (IsPaused)
                return;

            if (currentState == Constants.BODYSTATE.BLEND)
            {
                //save the positions of the animation to blend to
                if (!snapShotTaken) MakeSnapShot();

                //calculate blend time
                intRagdollBlendTime += Time.deltaTime / ragdollToAnimBlendTime;
                intRagdollBlendTime = Mathf.Clamp01(intRagdollBlendTime);
                SnapArmatureToRigidBones(intRagdollBlendTime);

                //blending is done > proceed to normal animations
                if (intRagdollBlendTime >= 1)
                {
                    //reset the position of the animator
                    animator.transform.position = glPOSITION;
                    animator.transform.rotation = glROTATION;
                    animator.enabled = true;
                    currentState = Constants.BODYSTATE.ANIMATION;
                }
            }
        }
        int skipFrame = 0;
        private void FixedUpdate()
        {
            if (IsPaused)
                return;

            if (currentState == Constants.BODYSTATE.ANIMATION)
            {
                if (IsRagdoll)
                    DeActivateRagdoll();

                if (RigidSnapWhileInAnimation)
                    SnapRigidBonesToArmature();

                skipFrame = 0;
            }
            else if (currentState == Constants.BODYSTATE.RAGDOLL && !IsRagdoll)
            {
                //Need to skip some frames so the RigidBodys are set before the Update is called
                //De-Sync problem between Update and FixedUpdate
                if (skipFrame < 2 && !RigidSnapWhileInAnimation)
                    SnapRigidBonesToArmature(true);
                else
                    ActivateRagdoll();

                skipFrame++;
            }
        }

        private void MakeSnapShot()
        {
            animator.enabled = false;


            //used to determine the direction this body is facing, no need for it now,
            //we don't use this feature in our game

            //direction via hip and head
            Vector3 direction = RigidHipTransform.position - RigidHeadTransform.position;
            direction.y = 0; direction = direction.normalized;
      
            //used to check the getup rotation
            bool faceDown = RigidHeadTransform.TransformDirection(Vector3.forward).y < 0;
            glROTATION = Quaternion.LookRotation(faceDown?-direction: direction);

            glPOSITION = RigidHipTransform.position;
            glPOSITION.y = animator.transform.position.y;

            animator.transform.position = glPOSITION;
            animator.transform.rotation = glROTATION;

            for (int i = 0; i < bodyParts.Count; i++)
            {
                bodyParts[i].savedPosition = connectors[i].bone.transform.position - glPOSITION;
                bodyParts[i].savedRotation = connectors[i].bone.transform.rotation * glROTATION;
            }
            intRagdollBlendTime = 0;

            snapShotTaken = true;
        }

        private void SnapArmatureToRigidBones(float ammount = -1)
        {


            //reverse list from deepest to highest (inverse kinematics, principle)
            Vector3 offsetPosition = currentState == Constants.BODYSTATE.RAGDOLL ? Vector3.zero : glPOSITION;
            Quaternion offsetRosition = currentState == Constants.BODYSTATE.RAGDOLL ? Quaternion.identity : glROTATION;
            for (int i = connectors.Count - 1; i >= 0; i--)
            {
                if (ammount == -1)
                {
                    connectors[i].bone.transform.position = bodyParts[i].transform.position + bodyParts[i].transform.TransformDirection(connectors[i].offsetPosition) + offsetPosition;
                    connectors[i].bone.transform.rotation = bodyParts[i].transform.rotation * Quaternion.Inverse(connectors[i].offsetRotation) * Quaternion.Inverse(offsetRosition);
                }
                else
                {
                    connectors[i].bone.transform.position = Vector3.Lerp(   bodyParts[i].transform.position + bodyParts[i].transform.TransformDirection(connectors[i].offsetPosition), 
                                                                            bodyParts[i].savedPosition + offsetPosition, ammount);
                    connectors[i].bone.transform.rotation = Quaternion.Slerp(bodyParts[i].transform.rotation * Quaternion.Inverse(connectors[i].offsetRotation), 
                                                                            bodyParts[i].savedRotation * Quaternion.Inverse(offsetRosition), ammount);
                }
            }
        }

        private void SnapRigidBonesToArmature(bool forced = false)
        {
            for (int i = 0; i < bodyParts.Count; i++)
            {
                if (!forced)
                {
                    bodyParts[i].myRigidBody.MovePosition(connectors[i].bone.transform.position - bodyParts[i].transform.TransformDirection(connectors[i].offsetPosition));
                    bodyParts[i].myRigidBody.MoveRotation(connectors[i].bone.transform.rotation * (connectors[i].offsetRotation));
                }
                else
                {
                    bodyParts[i].myRigidBody.position = connectors[i].bone.transform.position - bodyParts[i].transform.TransformDirection(connectors[i].offsetPosition);
                    bodyParts[i].myRigidBody.rotation = connectors[i].bone.transform.rotation * (connectors[i].offsetRotation);
                }
            }
            animator.transform.position = glPOSITION;
            animator.transform.rotation = glROTATION;
        }

        #region PUBLICS
        public void Pause(bool pause)
        {
            IsPaused = pause;

            if (currentState == Constants.BODYSTATE.RAGDOLL || currentState == Constants.BODYSTATE.BLEND)
            {
                for (int i = 0; i < bodyParts.Count; i++)
                    bodyParts[i].ActivatePhysics(!pause);
            }
            if (currentState == Constants.BODYSTATE.ANIMATION && animator != null)
            {
                animator.enabled = !pause;
            }
        }

        /// <summary>
        /// Trigger on start!
        /// To wake the BPM 
        /// </summary>
        public void ResetBodyPartManager()
        {
            IsRagdoll = false; //don't want blending if we are in rag-doll mode, instant animation mode.
            glPOSITION = rootTransform.position;
            glROTATION = rootTransform.rotation;
            DeActivateRagdoll(); //make rigid-bodies inactive and follow the animation
        }

        public void ToggleColliders(bool activate)
        {
            //cant deactivate colliders when in Ragdoll mode, this would result in sinking of the body
            if (!activate && currentState != Constants.BODYSTATE.ANIMATION)
                return;

            for (int i = 0; i < bodyParts.Count; i++)
                bodyParts[i].ActivateBodyPart(activate);
        }

        //TRIGGERED BY 'HEALTHBASE'
        public void ActivateRagdoll()
        {
            if (IsRagdoll)
                return;

            IsRagdoll = true;
            currentState = Constants.BODYSTATE.RAGDOLL;
            SnapRigidBonesToArmature(true);

            if (animator != null)
                animator.enabled = false;
            for (int i = 0; i < bodyParts.Count; i++)
                bodyParts[i].ActivatePhysics(true);
        }
        public void DeActivateRagdoll()
        {
            if (IsRagdoll) //blend to animation
            {
                snapShotTaken = false;
                currentState = Constants.BODYSTATE.BLEND;
            }
            else
                currentState = Constants.BODYSTATE.ANIMATION;

            IsRagdoll = false;
            if (animator != null)
                animator.enabled = true;
            for (int i = 0; i < bodyParts.Count; i++)
                bodyParts[i].ActivatePhysics(false);
        }

        public void ToAnimation(string animationState, float time)
        {
            if (IsRagdoll && time != -1) //blend to animation
            {
                snapShotTaken = false;
                currentState = Constants.BODYSTATE.BLEND;
            }
            else
                currentState = Constants.BODYSTATE.ANIMATION;

            IsRagdoll = false;
            if (animator != null)
                animator.enabled = true;

            animator.Play(animationState, 0);
            for (int i = 0; i < bodyParts.Count; i++)
                bodyParts[i].ActivatePhysics(false);
        }

        /*
        //Could be used to force the ragdoll to a new position
        public void Movement(Vector3 newPosition, Quaternion newRotation)
        {
            glPOSITION = newPosition;
            glROTATION = newRotation;
            if (currentState == Constants.BODYSTATE.RAGDOLL)
            {
                if (!bodyParts[rootRigidBone].myRigidBody.isKinematic)
                    bodyParts[rootRigidBone].myRigidBody.isKinematic = true;

                bodyParts[rootRigidBone].myRigidBody.MovePosition(glPOSITION);
                bodyParts[rootRigidBone].myRigidBody.MoveRotation(glROTATION);
            }
        }
        */
        #endregion

#if !NOT_EXCLUDED
        //this attached scripts may be empty and this class is not depended on it
        public event GlobalHit OnGlobalHit;

        private HealthBaseBPM healthBase;

        public void RegisterBodyPartHit(BodyPartBPM bodyPart)
        {
            if (healthBase == null)
                healthBase = GetComponent<HealthBaseBPM>();
            if (healthBase != null)
                bodyPart.OnBodyPartHit += healthBase.BodyPartHit;
        }
        public void UnRegisterBodyPartHit(BodyPartBPM bodyPart)
        {
            if (healthBase == null)
                healthBase = GetComponent<HealthBaseBPM>();
            if (healthBase != null)
                bodyPart.OnBodyPartHit -= healthBase.BodyPartHit;
        }
        public void GlobalImpact(Constants.ImpactInfo impactInfo, bool instantDeath = false)
        {
            if (OnGlobalHit != null)
                OnGlobalHit(impactInfo, instantDeath);

            if (healthBase == null)
                healthBase = GetComponent<HealthBaseBPM>();
            if (healthBase != null)
                healthBase.GlobalImpact(impactInfo, instantDeath);
        }
#else
        public void RegisterBodyPartHit(BodyPartBPM bodyPart){}
        public void UnRegisterBodyPartHit(BodyPartBPM bodyPart) { }
        public void GlobalImpact(Constants.ImpactInfo impactInfo, bool instantDeath = false) { }
#endif

#if UNITY_EDITOR
        public bool connectionTabShown { get; private set; }
        public void ToggleConnectionSettingsTab()
        {
            connectionTabShown = !connectionTabShown;
        }

        public void CreateFromList()
        {
            //TODO: make the connection process a bit more efficient
            //loop 2 times first create the bodies then link the bodies with each-other
            //easiest way to do this...
            for (int loop = 0; loop < 2; loop++)
            {
                bodyParts.Clear();
                List<BodySettings> tempList = new List<BodySettings>(bodySettingsList);

                while (tempList.Count > 0)
                {
                    BodySettings currentPart = tempList[tempList.Count - 1];
                    if (currentPart.depth == 0) //found the lowest depth (ROOT)
                    {
                        BodyPartBPM bPart = MakeBodyPart(currentPart.bone, currentPart.type, null);
                        bodyParts.Add(bPart);
                        tempList.Remove(currentPart);
                        break;
                    }

                    for (int i = tempList.Count - 1; i >= 0; i--)
                    {
                        BodySettings checkPart = tempList[i];
                        if (currentPart.depth - 1 == checkPart.depth) //getting the first bone thats one level higher and connect it
                        {
                            BodyPartBPM bPart = MakeBodyPart(currentPart.bone, currentPart.type, checkPart.bone);
                            bodyParts.Add(bPart);
                            tempList.Remove(currentPart);
                            break;
                        }
                    }
                }
            }
        }

        public void ConnectorsList(bool clear = false)
        {
            connectors.Clear();
            if (!clear)
            {
                //reverse the list as the body part list also is revered for heir achy reasons
                for (int i = bodySettingsList.Count - 1; i >= 0; i--)
                {
                    Vector3 oPos = bodySettingsList[i].bone.InverseTransformDirection(bodySettingsList[i].connectors.position - bodySettingsList[i].bone.position);
                    Quaternion cRot = bodySettingsList[i].connectors.rotation;
                    Quaternion bRot = bodySettingsList[i].bone.rotation;
                    Quaternion oRot = (Quaternion.Inverse(cRot) * (bRot));
                    connectors.Add(new Connection(bodySettingsList[i].connectors, oPos, oRot));
                }
                for (int i = 0; i < bodyParts.Count; i++)
                    bodyParts[i].SetHingeOffset(bodySettingsList[(bodyParts.Count - 1) - i].connectors);
            }
            else
            {
                for (int i = 0; i < bodySettingsList.Count; i++)
                {
                    bodySettingsList[i].connectors = null;
                }
            }
        }

        public void ClearAll(bool hardClear = false)
        {
            Joint[] jts = transform.GetComponentsInChildren<Joint>();
            foreach (Joint jt in jts)
                DestroyImmediate(jt);
            Rigidbody[] rbs = transform.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody rb in rbs)
                DestroyImmediate(rb);

            connectors.Clear();
            bodyParts.Clear();

            BodyPartBPM[] bps = transform.GetComponentsInChildren<BodyPartBPM>();
            foreach (BodyPartBPM bp in bps)
                DestroyImmediate(bp);

            BodyPartBPM[] bpAs = transform.GetComponentsInChildren<BodyPartBPM>();
            foreach (BodyPartBPM bpA in bpAs)
                DestroyImmediate(bpA);

            if (hardClear)
            {
                Collider[] cls = transform.GetComponentsInChildren<Collider>();
                foreach (Collider cl in cls)
                    DestroyImmediate(cl);

                bodySettingsList.Clear();
            }
        }

        private BodyPartBPM MakeBodyPart(Transform bone, Constants.BODYPART_TYPE partType, Transform connectionBone)
        {
            BodyPartBPM bp = bone.GetComponent<BodyPartBPM>();
            if (bp == null)
                bp = bone.gameObject.AddComponent<BodyPartBPM>();

            BodyPartBPM connection = null; //is null by default unless we pass a connection bone
            if (connectionBone != null)
            {
                connection = connectionBone.GetComponent<BodyPartBPM>();
                if (connection == null)
                    connection = connectionBone.gameObject.AddComponent<BodyPartBPM>();
            }

            if (bp != null)
            {
                bp.CreateObjects(this, partType, connection);
                return bp;
            }
            return null;
        }

        /// <summary>
        /// Get the max depth of the Hierarchy from the body-part list
        /// </summary>
        /// <returns></returns>
        public int GetMaxDepth()
        {
            int output = int.MinValue;
            for (int i = 0; i < bodySettingsList.Count; i++)
            {
                if (bodySettingsList[i].depth >= output)
                    output = bodySettingsList[i].depth;
            }
            return output;
        }

        private void OnDrawGizmos()
        {
            if (showWireAmature)
            {
                Gizmos.color = Color.cyan;
                for (int i = 0; i < bodyParts.Count; i++)
                {
                    if (bodyParts[i].connectedBodyPart != null)
                        Gizmos.DrawLine(bodyParts[i].transform.position, bodyParts[i].connectedBodyPart.transform.position);
                    Gizmos.DrawSphere(bodyParts[i].transform.position, .02f);
                }
            }
            if (showWireConnectors)
            {
                for (int i = 0; i < bodySettingsList.Count; i++)
                {
                    if (bodySettingsList[i].connectors != null)
                    {
                        Gizmos.color = Color.yellow;
                        Gizmos.DrawLine(bodySettingsList[i].bone.transform.position, bodySettingsList[i].connectors.position);
                        Gizmos.DrawSphere(bodySettingsList[i].connectors.position, .02f);
                        Gizmos.color = Color.blue;
                        Gizmos.DrawRay(bodySettingsList[i].connectors.position, bodySettingsList[i].connectors.TransformDirection(Vector3.up * .15f));

                    }
                    else if (bodySettingsList[i].bone != null)
                    {
                        Gizmos.color = Color.red;
                        Gizmos.DrawSphere(bodySettingsList[i].bone.transform.position, .04f);
                    }
                }
            }
        }
#endif
    }
}
