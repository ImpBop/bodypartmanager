﻿using UnityEngine;

namespace BodyPartManager
{
    public class BodyPartBPM : MonoBehaviour
    {
        public delegate void BodyPartHitEvent(Constants.ImpactInfo impactInfo, BodyPartBPM bodypart, bool instantDeath = false);
        public BodyPartHitEvent OnBodyPartHit;

        public Constants.BODYPART_TYPE partType = Constants.BODYPART_TYPE.CHEST;
        public ManagerBPM myManager;
        public Rigidbody myRigidBody;
        public CharacterJoint myCharacterJoint;
        public Collider myCollider;

        [HideInInspector]
        public BodyPartBPM connectedBodyPart;
        [HideInInspector]
        public Vector3 savedPosition;
        [HideInInspector]
        public Quaternion savedRotation;

        private bool isActive;
        private bool isParentBone;

        private void Start()
        {
            //double check, should be created before hand in the editor
            if (myCollider == null)
                myCollider = GetComponent<Collider>();
            if (myRigidBody == null)
                myRigidBody = GetComponent<Rigidbody>();

            if (myCharacterJoint == null)
                myCharacterJoint = GetComponent<CharacterJoint>();

            //Could assign LayerMask to BodyPart so its easier to hit with a ray-cast.
            //gameObject.layer = LayerMask.NameToLayer("BodyPart");

            isActive = true;
            ActivatePhysics(false);
            myManager.RegisterBodyPartHit(this);
        }

        private void OnDisable()
        {
            if (myManager != null)
                myManager.UnRegisterBodyPartHit(this);
        }

        #region Triggered By BodyPartManager in editor mode
        public void CreateObjects(ManagerBPM bodyPartManger, Constants.BODYPART_TYPE bodyPartType, BodyPartBPM connection)
        {
            partType = bodyPartType;
            myManager = bodyPartManger;
            connectedBodyPart = connection;
            isParentBone = (connectedBodyPart == null);

            myCollider = GetComponent<Collider>();
            if (myCollider == null)
                myCollider = gameObject.AddComponent<CapsuleCollider>();

            myRigidBody = GetComponent<Rigidbody>();
            if (myRigidBody == null)
                myRigidBody = gameObject.AddComponent<Rigidbody>();

            if (connection != null)
                Debug.Log("BP:" + this.gameObject.name + " :: " + (connection.gameObject.name ?? "No connection send"));
            else
                Debug.Log("BP:" + this.gameObject.name + " :: " + "No connection send");

            if (!isParentBone)
            {
                myCharacterJoint = GetComponent<CharacterJoint>();
                if (myCharacterJoint == null)
                {
                    myCharacterJoint = gameObject.AddComponent<CharacterJoint>();
                    myCharacterJoint.axis = new Vector3(0, 0, 1);
                }
                myCharacterJoint.enableProjection = true;
                myCharacterJoint.connectedBody = connectedBodyPart.myRigidBody;
            }
            else
            {
                myCharacterJoint = GetComponent<CharacterJoint>();

                //Remove the character joint connection and component if we don't have a connection or are a root body-part.
                if (myCharacterJoint != null)
                {
                    DestroyImmediate(myCharacterJoint);
                    myCharacterJoint = null;
                }
            }
        }

        public void SetHingeOffset(Transform bone)
        {
            myCharacterJoint = GetComponent<CharacterJoint>();
            if (myCharacterJoint != null)
            {
                Vector3 hingeOffset = myRigidBody.transform.InverseTransformPoint(bone.position);
                myCharacterJoint.autoConfigureConnectedAnchor = true;
                myCharacterJoint.connectedAnchor = Vector3.zero;
                myCharacterJoint.anchor = hingeOffset;
            }
        }
        #endregion

        public void ActivatePhysics(bool enabled)
        {
            myRigidBody.useGravity = enabled;
            myRigidBody.isKinematic = !enabled;
            if (enabled)
                myRigidBody.WakeUp();
            else
                myRigidBody.Sleep();


        }
        public void ActivateBodyPart(bool enabled)
        {
            isActive = enabled;

            if (enabled)
                myRigidBody.WakeUp();
            else
                myRigidBody.Sleep();
        }

        public void MakeTransformSnapShot()
        {
            savedPosition = transform.position;
            savedRotation = transform.rotation;
        }


        public void isShot(Constants.ImpactInfo impactInfo, bool instantDeath = false)
        {
            if (!isActive)
                return;

            if (OnBodyPartHit != null)
                OnBodyPartHit(impactInfo, this, instantDeath);
        }
    }
}
