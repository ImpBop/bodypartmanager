﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BodyPartManager
{
    public class Constants : MonoBehaviour
    {
        public enum BODYSTATE { ANIMATION, RAGDOLL, BLEND, NONE };

        /// <summary>
        /// Definition of the different body types, 
        /// there values are a representation of the damage taken when shot at.
        /// </summary>
        public enum BODYPART_TYPE
        {
            ROOT = 25, //HIP
            HEAD = 100,
            SPINE = 35,
            CHEST = 30,
            ARM_UPPER = 10,
            ARM_LOWER = 8,
            HAND = 5,
            LEG_UPPER = 9,
            LEG_LOWER = 7,
            FOOT = 6,
            TAIL = 4
        }

        public class ImpactInfo
        {
            public Transform SenderTransform { private set; get; }
            public float DamageForce    { private set; get; }

            public float ExplosionRadius { private set; get; }
            public float ExplosionForce  { private set; get; }

            //Calculate on Impact
            public Vector3 SenderInitPosition { private set; get;  }
            public Vector3 ImpactPoint { private set; get; }

            public ImpactInfo(Transform sender, float damageForce)
            {
                SenderTransform = sender;
                SenderInitPosition = SenderTransform.position;
                DamageForce = damageForce;
                
                ExplosionRadius = 1;
                ExplosionForce = -1;
            }

            public ImpactInfo(Transform sender, float damageForce, float explosionForce, float explosionRadius)
            {
                SenderTransform = sender;
                SenderInitPosition = SenderTransform.position;
                DamageForce = damageForce;

                ExplosionForce = explosionForce;
                ExplosionRadius = explosionRadius;
            }

            public void OnImpact(Vector3 hitPoint)
            {
                ImpactPoint = hitPoint;
            }

            public bool IsExplosionForce { get { return (ExplosionForce != -1 && ExplosionRadius != -1); }}

            public Vector3 GetDirection()
            {
                return (ImpactPoint - SenderInitPosition).normalized;
            }

            public Vector3 GetImpactForce()
            {
                return GetDirection() * DamageForce;
            }

            public float GetDistance()
            {
                return Vector3.Distance(SenderInitPosition, ImpactPoint);
            }

            public float GetRadiusForce(Vector3 observerPosition)
            {

                float relativeDistance = (ExplosionRadius / GetDistance()) / ExplosionRadius;
                return Mathf.Clamp01(relativeDistance) * ExplosionForce;
            }
        }
    }
}